let firstNumber;
let secondNumber;
let mathSign;
let result;

function calcNum(num1, num2, inputMath) {
    let result;
    switch (inputMath) {
        case "+":
            result = num1 + num2; // get a math value
            break;
        case "-":
            result = num1 - num2;
            break;
        case "*":
            result = num1 * num2;
            break;
        case "/":
            result = num1 / num2;
            break;
        default:
            result = null;
            break;
    }
    return result;
}
//--------------------------prompt----------------------------
function getSign() {
    return prompt("Enter symbol of (+,-,*,/)");
}

function getValue(text, value) {
    return prompt(`Enter ${text} number`, value);
}

function setMath() {
    let result;
    do {
        let inputMath = getSign();
        if (inputMath === null) {
            console.log("You canceled programm");
            break;
        } else if (
            inputMath == "+" ||
            inputMath == "-" ||
            inputMath == "*" ||
            inputMath == "/"
        ) {
            result = inputMath;
        }
    } while (!result);
    return result;
}

function setNumber(text, num) {
    do {
        num = getValue(text, num);
        // checker
        if (num === null) {
            console.log("You canceled programm");
            break;
        } else if (num === "" || num[0] === " ") {
            num = undefined;
        } else if (isFinite(num)) {
            num = Number(num);
        }
    } while (!isFinite(num));
    return num;
}
//----------------checkers------------------------
firstNumber = setNumber("first", firstNumber);

if (Number.isFinite(firstNumber)) {
    secondNumber = setNumber("second", secondNumber);
}
if (Number.isFinite(secondNumber)) {
    mathSign = setMath();
}

if (Number.isFinite(firstNumber) && Number.isFinite(secondNumber)) {
    result = calcNum(firstNumber, secondNumber, mathSign);
}

if (Number.isFinite(result)) {
    console.log(`Result: ${firstNumber} ${mathSign} ${secondNumber} = ${result}`);
} else {
    console.log("Error calculate");
}